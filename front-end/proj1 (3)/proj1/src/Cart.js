import React, { useContext } from "react";
import { newContext } from "./App";
import './index.css';

export default function ViewCart(props) {
  const contextObj = useContext(newContext);
  const { cartItems, setCartItems } = contextObj;

  const getTotalPrice = () => {
    let total = 0;
    cartItems.forEach(item => {
      total += item.price * item.qty;
    });
    return total;
  };

  return (
    <div className="cart-container">
      <h2>Cart Items</h2>
      <div className="cart-items">
        {cartItems.length > 0 ? (
          cartItems.map((item, index) => (
            <div key={index} className="cart-item">
              <p>{item.productName}</p>
              <p>Qty: {item.qty}</p>
              <p>Price: Rs {item.price}</p>
              <button
                onClick={() => {
                  const newCartItems = [...cartItems];
                  newCartItems[index].qty -= 1;
                  if (newCartItems[index].qty === 0) {
                    newCartItems.splice(index, 1);
                  }
                  setCartItems(newCartItems);
                }}
                className="btn btn-warning"
              >
                -
              </button>
              <button
                onClick={() => {
                  const newCartItems = [...cartItems];
                  newCartItems[index].qty += 1;
                  setCartItems(newCartItems);
                }}
                className="btn btn-warning"
              >
                +
              </button>
            </div>
          ))
        ) : (
          <p>No items in cart</p>
        )}
        <div className="cart-total">
          <p>Total: Rs {getTotalPrice()}</p>&nbsp;&nbsp;
          <button onClick={() => navigate('/checkout')} className="btn btn-success">
            Buy Now
          </button>
        </div>
      </div>
    </div>
  );
}
