package UserDaoPack;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import UserPack.User;

//import com.ecommerce.login.User;

//import com.ecommerce.products.Products;

public class UserDao {
	
	public List<User> getProducts()
	{
		List<User> userList=new ArrayList<>();
		User u = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce", "root", "Gopal@1This");
			
			Statement stmt = con.createStatement();
			String s = ("SELECT * FROM users");
			ResultSet rs = stmt.executeQuery(s);
			
			 while(rs.next()) {
				String username =rs.getString("username");
				String password =rs.getString("password");
				u = new User(username,password);
				userList.add(u);
				
			}
		rs.close();
		stmt.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		
		return userList;
		
	}
	
	public User login(String username,String password) throws ClassNotFoundException{
		User user  = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce", "root", "Gopal@1This");
			PreparedStatement ps = conn.prepareStatement("select * from users where user_name = ? and password = ?");
			ps.setString(1, username);
			ps.setString(2, password);
			
			ResultSet rs = ps.executeQuery();
			if (rs.next() == true) {
				user = new User(rs.getString(1),rs.getString(2));
				System.out.println();
			}else if(rs.next() == false) {
				System.out.println("Invalid User , Kindly check the Username or Password");
			}
			rs.close();
			ps.close();
			return user;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return user;
	}
	
}
